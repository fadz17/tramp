const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
// const Chatkit = require('pusher-chatkit-server')

const app = express()
// const chatkit = new Chatkit.default({
//   instanceLocator:'v1:us1:5e25fe59-08fc-4ed7-8715-1c9ada9734cd',
//   key:'f9fb8ee2-d176-4ae3-b619-552ca68e4db2:RXl3NsKvOow6BExxsZNN7othbr+b9YrLqcdwUARWITs=',
// })

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

// app.post('/users',(req,res)=>{
//   const{username}=req.body
  
//   chatkit

//   .createUser({
//     name: username,
//     id: username
//   })
//   .then(() => res.sendStatus(201))
//   .catch(error => {
//     if (error.error_type === 'services/chatkit/user_already_exists'){
//       res.sendStatus(200)

//     } else {
//       res.status(error.statusCode).json(error)

//     }

//   })
// })
const PORT = 3001
app.listen(PORT, err => {
  if (err) {
    console.error(err)
  } else {
    console.log(`Running on port ${PORT}`)
  }
})
